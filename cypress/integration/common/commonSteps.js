import { Given } from "cypress-cucumber-preprocessor/steps";
import { When } from "cypress-cucumber-preprocessor/steps";
import { Then } from "cypress-cucumber-preprocessor/steps";
/*******************************************Given****************************************** */
Given('I visit the url of the castorEdc', () => {
    cy.visit('/', { retryOnNetworkFailure: true });
})

/********************************************When****************************************** */

When('I click on {string} button', (button) => {
    switch (button) {
        case 'Login':
            cy.get('.Button').within(() => {
                cy.contains(' Log in').should('be.visible').click();
            });
            break;
        case 'Example Study: Familiarise Yourself With Castor':
            cy.get('.StudyTitle').within(() => {
                cy.contains('Familiarise Yourself With Castor').click();
            });
            break;
        case 'New':
            cy.get('button[id="create_record_button"]').within(() => {
                cy.contains('New').click();
            });
            break;
        case 'Create':
            cy.get('button[type="submit"]').within(() => {
                cy.contains('Create').click();
            });
            break;
    }

})

When('I enter {string} and {string} in {string} form', (email, password, formName) => {
    switch (formName) {
        case 'Log in':
            cy.get('input[id="field-username"]').type(email);
            cy.get('input[id="field-password"]').type(password);
            break;
    }
})

/********************************************Then******************************************** */
Then('I should {string} logged in castorEdc', (login) => {

    switch (login) {
        case 'be':
            cy.get('.StudyTitle').within(() => {
                cy.contains('Familiarise Yourself With Castor').should('be.visible');
            }); break;
        case 'not be':
            cy.get('.Button').within(() => {
                cy.contains(' Log in').should('be.visible');
            }); break;
    }

})