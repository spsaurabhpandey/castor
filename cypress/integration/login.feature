Feature: User account creation and login

    As a registered user, I want to be able to login to Castor Electronic Data Capture application


    #@ignore tag can be used to ignore a particular scenario during test run.

    Scenario Outline: login to castor using valid credentials
        Given I visit the url of the castorEdc
        When I enter "<email>" and "<password>" in "Log in" form
        And I click on "Login" button
        Then I should "be" logged in castorEdc
        Examples:
            | email                     | password   |
            | spsaurabhpandey@gmail.com | Castor@123 |