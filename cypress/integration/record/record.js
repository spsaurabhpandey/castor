import { Given } from "cypress-cucumber-preprocessor/steps";
import { When } from "cypress-cucumber-preprocessor/steps";
import { Then } from "cypress-cucumber-preprocessor/steps";
/*******************************************Given****************************************** */


/********************************************When****************************************** */

When('I select Institute', () => {
    cy.contains('Main Institute').click({ force: true });
    cy.get('#react-select-3-option-1').click({ force: true });
})


Then('fields related to {string} should be displayed', (form) => {
    switch (form) {
        case 'Consent':
            cy.contains('Informed Consent and Inclusion').should('be.visible');
            cy.contains('1. Consent').should('be.visible');
            break;
    }

})