Feature: Create new record within "familiarise yourself with Castor"

    As a user of CastorEdc, I want to be able to use "familiarise yourselfwith castor" functionality and create a new record.

    Background: user is logged in
        Given I visit the url of the castorEdc
        When I enter "spsaurabhpandey@gmail.com" and "Castor@123" in "Log in" form
        And I click on "Login" button
        Then I should "be" logged in castorEdc

    Scenario: Check if a user is able to create a new record
        When I click on "Example Study: Familiarise Yourself With Castor" button
        And I click on "New" button
        And I select Institute
        And I click on "Create" button
        Then fields related to "Consent" should be displayed