# castor

This project contains some UI tests for Castor Electronic Data Capture application

# How to run the cypress tests in local desktop?
<br>Step1: git clone https://gitlab.com/spsaurabhpandey/castor.git
<br>Step2: open the contents of this repository in an IDE (ex: VS Code).
<br>Step3: Run following commands in terminal
<br>a) npm install
<br>b) npx cypress open -> choose your browser and execute any .feature file.

# Where can I find scenario(s) in Gherkin?
<br>The feature files are at cypress/integration/

# Where and how can I find the report of test Execution?
<br>Run following commands:
<br>a) "npx cypress run" or "npm run headlessChrome"
<br>b) npm run report
<br>Latest html report will be placed at cypress/cucumber-report/index.html